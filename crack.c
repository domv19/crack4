#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "entry.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file rainbow_file\n", argv[0]);
        exit(1);
    }

    // TODO: Open the text hashes file for reading
    FILE *hf = fopen("hashes.txt", "r");
    // TODO: Check to make sure file was successfully opened
    if (!hf)
    {
        printf("Can't open text hashes file for reading\n");
        exit(1);
    }
    // TODO: Open the binary rainbow file for reading
    FILE *rf = fopen("rainbow.bow", "rb");
    // TODO: Check to make sure if was successfully opened
    if (!rf)
    {
        printf("Can't open binary rainbow file for reading\n");
        exit(1);
    }
    // TODO: This is a long list of things to do, so
    //       break it up into functions!
    // For each hash (read one line at a time out of the file):
    struct entry e;
    char hash[HASH_LEN];
    while (fgets(hash, HASH_LEN, hf) != NULL)
    {
        //Convert hex string to digest (use hex2digest. See md5.h)
        unsigned char *dig = hex2digest(hash);
        //Extract the first three bytes
        int idx = dig[0] * 65536 + dig[1] * 256 + dig[2];
        //Use those three bytes as a 24-bit number
        int file_location = idx;
        struct entry tmp;
        //Seek to that location in the rainbow file
        fseek(rf, file_location, SEEK_SET);
        //Read the entry found there
        //check to see if it's the one you want.
        //If not, read the next one
        //Repeat until you find the one you want.
        int read_result;
        do
        {
            read_result = fread(&tmp, 36, 1, rf);
        }
        while (memcmp(hash, (const void *)read_result, 3) != 0);
        
        //Display the hash and the plaintext
        printf("%s  %s\n", hash, e.pass);
    }
    fclose(hf);
    fclose(rf);
}
